class AddColumnsToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :name, :string
    add_column :users, :fullname, :string
    add_column :users, :website, :string
    add_column :users, :introduction, :text
    add_column :users, :phone, :string
    add_column :users, :sex, :boolean
  end
end
