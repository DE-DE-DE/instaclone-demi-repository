# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


#リスト 10.43

# サンプルユーザーを3人作成
# !をつけることでバリデーション通らなかったときに意図的に例外を発生させて、その後のrakeタスクを実行させないようにする

Pikachu = User.create!(           fullname: "G Gu",
                                      name: "Gu",
                                     email: "gu@example.com",
                              introduction: "グーです。チョキには強くて、パーには弱い。",
                                  password: "foobar",
                     password_confirmation: "foobar",
                                    avatar: open("./db/fixtures/avatar/Gu.jpg")
)

Pichu = User.create!(             fullname: "C Choki",
                                      name: "Choki",
                                     email: "choki@example.com",
                              introduction: "チョキです。パーには強くて、グーには弱い",
                                  password: "foobar",
                     password_confirmation: "foobar",
                                    avatar: open("./db/fixtures/avatar/Choki.jpg")
)

Raichu = User.create!(            fullname: "P Pa",
                                      name: "Pa",
                                     email: "pa@example.com",
                              introduction: "パーです。グーには強くて、チョキには弱い",
                                  password: "foobar",
                     password_confirmation: "foobar",
                                    avatar: open("./db/fixtures/avatar/Pa.jpg")
)

# 各ユーザーが10枚の写真を投稿し他のユーザーがそれにコメント
users = User.order(:created_at).take(3)#最初の3人
other_users = User.order(:created_at).take(3)#最初の3人
i = 0


10.times do
  users.each do |user|
    #写真を投稿
    # description = Faker::Lorem.sentence(word_count: 5)
    user.posts.create!(
        title: "投稿#{i}",
        image: open("./db/fixtures/image/#{i}.jpg"),
        description: "投稿#{i}への説明文です。投稿#{i}への説明文です。投稿#{i}への説明文です。"
    )

    other_users.each do |other_user|
      unless other_user == Post.first.user

        #他のユーザーによるコメント
        # content = Faker::Lorem.sentence(word_count: 2)
        other_user.comments.create!(
            content: "コメントです。コメントです。",
            post_id: Post.first.id
        )
        #コメントが付いたことを通知
        Post.first.create_notification_comment(other_user)

        #他のユーザーによるいいね
        other_user.favorites.create!(
            post_id: Post.first.id
        )
        #いいねが付いたことを通知
        Post.first.create_notification_like(other_user)
      end
    end

    i+=1
  end
end


#相互フォロー
def follow_each_other(user1, user2, user3)
  user1.follow(user2)#フォロー
  user2.create_notification_follow(user1)#通知
  user1.follow(user3)#フォロー
  user3.create_notification_follow(user1)#通知
end
follow_each_other(User.first, User.second, User.third)
follow_each_other(User.third, User.first, User.second)
follow_each_other(User.second, User.third, User.first)





