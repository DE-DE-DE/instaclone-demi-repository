require "rails_helper"
describe "アカウントテスト", type: :system do
  describe "ユーザー登録" do

    context "情報を正しく入力" do
      before do
        visit new_user_registration_path
        fill_in "名前", with: "G Gu"
        fill_in "ユーザーネーム", with: "Gu"
        fill_in "メールアドレス", with: "gu@example.com"
        fill_in "パスワード", with: "foobar"
        fill_in "確認用パスワード", with: "foobar"
        check "利用規約に同意する"
        click_button "登録"
      end

      it "ログイン中のユーザーが表示されること" do
        expect(page).to have_content "Gu"
      end
    end


    context "空の情報を入力" do
      before do
        visit new_user_registration_path
        fill_in "名前", with: ""
        fill_in "ユーザーネーム", with: ""
        fill_in "メールアドレス", with: ""
        fill_in "パスワード", with: ""
        fill_in "確認用パスワード", with: ""
        check "利用規約に同意する"
        expect(page).to have_css("#btn1:enabled")#https://www.it-swarm.dev/ja/ruby-on-rails/%E3%83%9C%E3%82%BF%E3%83%B3%E3%81%AE%E5%AD%98%E5%9C%A8%E3%81%A8%E3%81%9D%E3%81%AE%E6%9C%89%E5%8A%B9%E7%8A%B6%E6%85%8B%E3%81%BE%E3%81%9F%E3%81%AF%E7%84%A1%E5%8A%B9%E7%8A%B6%E6%85%8B%E3%82%92%E3%83%81%E3%82%A7%E3%83%83%E3%82%AF%E3%81%99%E3%82%8B%E3%82%AB%E3%83%94%E3%83%90%E3%83%A9%E3%82%A2%E3%82%B5%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3%E3%82%92%E4%BD%9C%E6%88%90%E3%81%99%E3%82%8B%E3%81%AB%E3%81%AF%E3%81%A9%E3%81%86%E3%81%99%E3%82%8C%E3%81%B0%E3%82%88%E3%81%84%E3%81%A7%E3%81%99%E3%81%8B%EF%BC%9F/1069074691/
        click_button "登録"
      end

      it "エラーメッセージが表示されること" do
        expect(page).to have_content "Nameを入力してください"
        expect(page).to have_content "メールアドレスを入力してください"
        expect(page).to have_content "メールアドレスは不正な値です"
        expect(page).to have_content "パスワードを入力してください"
      end
    end


    context "不正な情報を入力" do
      before do
        visit new_user_registration_path
        fill_in "名前", with: "a" * 21
        fill_in "ユーザーネーム", with: "a" * 21
        fill_in "メールアドレス", with: ("a" * 256) + "@foo"
        fill_in "パスワード", with: "foo"
        fill_in "確認用パスワード", with: "foo"
        check "利用規約に同意する"
        expect(page).to have_css("#btn1:enabled")#https://www.it-swarm.dev/ja/ruby-on-rails/%E3%83%9C%E3%82%BF%E3%83%B3%E3%81%AE%E5%AD%98%E5%9C%A8%E3%81%A8%E3%81%9D%E3%81%AE%E6%9C%89%E5%8A%B9%E7%8A%B6%E6%85%8B%E3%81%BE%E3%81%9F%E3%81%AF%E7%84%A1%E5%8A%B9%E7%8A%B6%E6%85%8B%E3%82%92%E3%83%81%E3%82%A7%E3%83%83%E3%82%AF%E3%81%99%E3%82%8B%E3%82%AB%E3%83%94%E3%83%90%E3%83%A9%E3%82%A2%E3%82%B5%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3%E3%82%92%E4%BD%9C%E6%88%90%E3%81%99%E3%82%8B%E3%81%AB%E3%81%AF%E3%81%A9%E3%81%86%E3%81%99%E3%82%8C%E3%81%B0%E3%82%88%E3%81%84%E3%81%A7%E3%81%99%E3%81%8B%EF%BC%9F/1069074691/
        click_button "登録"
      end

      it "エラーメッセージが表示されること" do
        expect(page).to have_content "Nameは20文字以内で入力してください"
        expect(page).to have_content "Fullnameは20文字以内で入力してください"
        expect(page).to have_content "メールアドレスは255文字以内で入力してください"
        expect(page).to have_content "メールアドレスは不正な値です"
        expect(page).to have_content "パスワードは6文字以上で入力してください"
      end
    end

    context "利用規約にチェックをいれないとき" do
      before do
        visit new_user_registration_path
      end

      it "登録ボタンを押せないこと" do
        expect(page).to have_css("#btn1:disabled")
      end
    end

  end
end