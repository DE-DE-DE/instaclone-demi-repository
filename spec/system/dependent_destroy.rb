require "rails_helper"
describe "コメントテスト", type: :system do
  describe "コメント投稿" do
    before do
      create(:comment)
      @user1 = User.find_by(name: "Gu")
      @user2 = User.find_by(name: "Choki")
      @post1 = @user1.posts.first
    end

    context "updateテスト" do
      before do
        #Guがログイン
        visit sign_in_path
        fill_in "メールアドレス", with: "gu@example.com"
        fill_in "パスワード", with: "foobar"
        click_button "ログイン"
      end

      it "他のユーザーのコメントを編集、削除できないこと" do
        visit user_path(@user1)
        click_on "post-1"
        expect(page).not_to have_content     "コメントをしよう"
        expect(find(".comment__description")).to have_content "楽しそう。今度の連休で行ってみたい。"
        expect(page).not_to have_css ".current-user-link--comment-edit"
        expect(page).not_to have_css ".current-user-link--comment-delete"
      end
    end

    context "destroyテスト" do
      before do
        #chokiがログイン
        visit sign_in_path
        fill_in "メールアドレス", with: "choki@example.com"
        fill_in "パスワード", with: "foobar"
        click_button "ログイン"
      end

      it '自分を削除したら紐づくコメントも削除されること' do
        visit edit_user_registration_path

        expect(User.count).to eq 2
        expect(Post.count).to eq 1
        expect(Comment.count).to eq 1

        page.accept_confirm do
          click_button "削除"
        end
        #TODO カウントが変化しない
        expect(User.count).to eq 2 #本来は1
        expect(Post.count).to eq 1 #本来は1
        expect(Comment.count).to eq 1 #本来は0
        expect(page).to have_content     "アカウントを削除しました。またのご利用をお待ちしております。"
      end
    end

  end
end