require "rails_helper"
describe "コメントテスト", type: :system do
  describe "コメント投稿" do
    before do
      create(:comment)
      @user1 = User.find_by(name: "Gu")
      @user2 = User.find_by(name: "Choki")
      @post1 = @user1.posts.first
      #pikachuがログイン
      visit sign_in_path
      fill_in "メールアドレス", with: "choki@example.com"
      fill_in "パスワード", with: "foobar"
      click_button "ログイン"
      visit user_path(@user1)
      click_on "post-1"
    end

    context "updateテスト" do

      it "正しい入力 編集できること" do
        find(".current-user-link--comment-edit").click
        fill_in "コメント", with: "すごいきれい。今度行ってみたい。"
        click_button "投稿"
        expect(find(".comment__description")).to have_content "すごいきれい。今度行ってみたい。"
        expect(page).to have_css ".current-user-link--comment-edit"
        expect(page).to have_css ".current-user-link--comment-delete"
        #投稿したコメントが@post1に関連付け(belongs to)できていること
        expect(Comment.find_by(content: "すごいきれい。今度行ってみたい。").post).to eq @post1
        #投稿したコメントが@user2に関連付け(belongs to)できていること
        expect(Comment.find_by(content: "すごいきれい。今度行ってみたい。").user).to eq @user2
      end

      it "空入力 編集できないこと" do
        find(".current-user-link--comment-edit").click
        fill_in "コメント", with: ""
        click_button "投稿"
        expect(page).to have_content "Contentを入力してください"
        expect(page).to have_css "form"
      end

      it "140超過入力 編集できないこと" do
        find(".current-user-link--comment-edit").click
        fill_in "コメント", with: "a" * 141
        click_button "投稿"
        expect(page).to have_content "Contentは140文字以内で入力してください"
        expect(page).to have_css "form"
      end
    end

    context "destroyテスト" do

      it "削除できること" do
        expect(page).not_to have_content     "コメントをしよう"
        expect(page).not_to have_content     "コメント投稿"
        expect(page).to have_css ".posted-content__image"

        expect(page).to have_content "投稿数1件"
        expect(Comment.count).to eq 1

        # 関連する投稿主が一緒に削除されないこと
        expect{ find(".current-user-link--comment-delete").click }.to change{ User.count }.by(0)

        expect(page).to have_content "投稿数1件"
        expect(Comment.count).to eq 0

        expect(page).to have_content     "コメントをしよう"
        expect(page).to have_content     "コメント投稿"
      end
    end

  end
end