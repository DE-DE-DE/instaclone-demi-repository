require "rails_helper"
describe "画像投稿テスト", type: :system do
  describe "画像投稿" do
    before do
      @user1 = create(:user)
      visit sign_in_path
      fill_in "メールアドレス", with: "gu@example.com"
      fill_in "パスワード", with: "foobar"
      click_button "ログイン"
    end

    context "正しい投稿 アップロードできること" do
      before do
        visit new_post_path
        fill_in "タイトル", with: "湖"
        page.attach_file("app/assets/images/lake.jpg") do
          page.find('.file-field-label').click #クリックをトリガーにしてファイルを添付
        end
        fill_in "ひとこと", with: "湖でボート乗ってきた"
        click_button "アップロード"
      end

      it "投稿画像が表示されること" do
        expect(page).to have_css ".posted-content__image"
        expect(page).to have_content "投稿数1件"
        expect(Post.find_by(title: "湖").user).to eq @user1 #投稿した画像が@user1に関連付けできていること
      end
    end

    context "不正な投稿" do
      before do
        visit new_post_path
        fill_in "post[title]", with: ""
        fill_in "ひとこと", with: ""
        click_button "アップロード"
      end

      it "投稿できないこと" do
        expect(page).to have_content "投稿できませんでした"
        expect(page).to have_content "Titleを入力してください"
        # ファイルを選択していないことに対するメッセージが未実装
      end
    end


    # context "空の情報を入力" do
    #   before do
    #     visit new_user_registration_path
    #     fill_in "名前", with: ""
    #     fill_in "ユーザーネーム", with: ""
    #     fill_in "メールアドレス", with: ""
    #     fill_in "パスワード", with: ""
    #     fill_in "確認用パスワード", with: ""
    #     check "利用規約に同意する"
    #     expect(page).to have_css("#btn1:enabled")#https://www.it-swarm.dev/ja/ruby-on-rails/%E3%83%9C%E3%82%BF%E3%83%B3%E3%81%AE%E5%AD%98%E5%9C%A8%E3%81%A8%E3%81%9D%E3%81%AE%E6%9C%89%E5%8A%B9%E7%8A%B6%E6%85%8B%E3%81%BE%E3%81%9F%E3%81%AF%E7%84%A1%E5%8A%B9%E7%8A%B6%E6%85%8B%E3%82%92%E3%83%81%E3%82%A7%E3%83%83%E3%82%AF%E3%81%99%E3%82%8B%E3%82%AB%E3%83%94%E3%83%90%E3%83%A9%E3%82%A2%E3%82%B5%E3%83%BC%E3%82%B7%E3%83%A7%E3%83%B3%E3%82%92%E4%BD%9C%E6%88%90%E3%81%99%E3%82%8B%E3%81%AB%E3%81%AF%E3%81%A9%E3%81%86%E3%81%99%E3%82%8C%E3%81%B0%E3%82%88%E3%81%84%E3%81%A7%E3%81%99%E3%81%8B%EF%BC%9F/1069074691/
    #     click_button "登録"
    #   end
    #
    #   it "エラーメッセージが表示されること" do
    #     expect(page).to have_content "Nameを入力してください"
    #     expect(page).to have_content "Fullnameを入力してください"
    #     expect(page).to have_content "メールアドレスを入力してください"
    #     expect(page).to have_content "メールアドレスは不正な値です"
    #     expect(page).to have_content "パスワードを入力してください"
    #   end
    # end

    # context "利用規約にチェックをいれないとき" do
    #   before do
    #     visit new_user_registration_path
    #   end
    #
    #   it "登録ボタンを押せないこと" do
    #     expect(page).to have_css("#btn1:disabled")
    #   end
    # end


  end
end