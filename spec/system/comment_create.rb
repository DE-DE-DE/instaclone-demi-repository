require "rails_helper"
describe "コメントテスト", type: :system do
  describe "コメント投稿" do
    before do
      create(:post_lake)#pichuの投稿
      create(:other_user)
      @user1 = User.find_by(name: "Gu")
      @user2 = User.find_by(name: "Choki")
      @post1 = @user1.posts.first
    end

    context "コメントのバリデーションテスト" do
      before do
        #chokiがログイン
        visit sign_in_path
        fill_in "メールアドレス", with: "choki@example.com"
        fill_in "パスワード", with: "foobar"
        click_button "ログイン"
        visit user_path(@user1)
        click_on "post-1"
      end

      it "コメントを投稿できること 編集 削除リンクがあること" do
        expect(page).to have_content     "コメントをしよう"
        click_link "コメント投稿"
        fill_in "コメント", with: "今度行ってみたい"
        click_button "投稿"
        expect(find(".comment__description")).to have_content "今度行ってみたい"
        expect(page).to have_css ".current-user-link--comment-edit"
        expect(page).to have_css ".current-user-link--comment-delete"
        #投稿したコメントが@post1に関連付け(belongs to)できていること
        expect(Comment.find_by(content: "今度行ってみたい").post).to eq @post1
        #投稿したコメントが@user2に関連付け(belongs to)できていること
        expect(Comment.find_by(content: "今度行ってみたい").user).to eq @user2
      end

      it "空コメントを投稿できないこと" do
        click_link "コメント投稿"
        fill_in "コメント", with: ""
        click_button "投稿"
        expect(page).to have_content "Contentを入力してください"
        expect(page).to have_css "form"
      end

      it "140超過コメントを投稿できないこと" do
        click_link "コメント投稿"
        fill_in "コメント", with: "a" * 141
        click_button "投稿"
        expect(page).to have_content "Contentは140文字以内で入力してください"
        expect(page).to have_css "form"
      end

    end

  end
end