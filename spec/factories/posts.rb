FactoryBot.define do
  factory :post_lake, class: Post do
    title {"湖"}
    image {  Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/lake.jpg'), 'image/png') }
    description {"ボート乗ってきた"}
    association :user, factory: :user
  end
  factory :post_prague, class: Post do
    title {"プラハ"}
    image {  Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/prague.jpg'), 'image/png') }
    description {"見晴らし最高"}
    association :user, factory: :other_user
  end
end