FactoryBot.define do
  factory :user, class: User do
    fullname {"G Gu"}
    name { "Gu" }
    email {"gu@example.com"}
    password { "foobar" }
    password_confirmation {"foobar"}
  end

  factory :other_user, class: User do
    fullname {"C Choki"}
    name { "Choki" }
    email {"choki@example.com"}
    password { "foobar" }
    password_confirmation {"foobar"}
  end

end