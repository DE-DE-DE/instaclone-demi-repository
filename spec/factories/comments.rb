FactoryBot.define do
  factory :comment do
    content { "楽しそう。今度の連休で行ってみたい。" }
    association :post, factory: :post_lake
    association :user, factory: :other_user
  end
end
