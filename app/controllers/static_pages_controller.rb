class StaticPagesController < ApplicationController
  skip_before_action :authenticate_user!, only: [:home, :agreement]

  def home
    @user = User.first
  end

  def agreement
  end

  private

  def user_params
    params.require(:user).permit(:fullname, :name, :email)
  end
end
