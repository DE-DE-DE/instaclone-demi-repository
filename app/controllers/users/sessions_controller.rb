# frozen_string_literal: true

class Users::SessionsController < Devise::SessionsController
  # before_action :configure_sign_in_params, only: [:create]

  # GET /resource/sign_in
  def new
    super
  end

  # POST /resource/sign_in
  def create
    store_location
    super
  end

  # DELETE /resource/sign_out
  def destroy
    super
  end

  # ログイン後のリダイレクト先
  def after_sign_in_path_for(resource)
    session[:forwarding_url].nil? ? user_path(resource) : redirect_back_page
  end

  # ログアウト後のリダイレクト先
  def after_sign_out_path_for(resource)
    home_path
  end

  # protected

  # If you have extra params to permit, append them to the sanitizer.
  # def configure_sign_in_params
  #   devise_parameter_sanitizer.permit(:sign_in, keys: [:attribute])
  # end
end
