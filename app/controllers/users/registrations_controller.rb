# frozen_string_literal: true

class Users::RegistrationsController < Devise::RegistrationsController
  before_action :configure_sign_up_params, only: [:create]
  before_action :configure_account_update_params, only: [:update]

  def index
    @user = User.find_by(id: params[:userid])
  end

  def show
    @user = User.find(params[:id])
    @posts = Kaminari.paginate_array(@user.posts).page(params[:page]).per(9)
    @favorite_posts = Kaminari.paginate_array(@user.favorites.map(&:post)).
      page(params[:page]).per(9)
    # 通知ページでクリックした投稿
    @post_with_quick_modal = Post.find_by(id: params[:post_with_quick_modal])
    session[:test1] = @user.name

  end

  # GET /resource/sign_up
  def new
    super
  end

  # POST /resource
  def create
    super
  end

  # GET /resource/edit
  def edit
    super
  end

  # PUT /resource
  def update
    super
  end

  # DELETE /resource
  def destroy
    super
  end

  # GET /resource/cancel
  # Forces the session data which is usually expired after sign
  # in to be expired now. This is useful if the user wants to
  # cancel oauth signing in/up in the middle of the process,
  # removing all OAuth session data.
  # def cancel
  #   super
  # end

  protected

  # If you have extra params to permit, append them to the sanitizer.
  def configure_sign_up_params
    devise_parameter_sanitizer.permit(:sign_up, keys: [:uid, :provider, :name, :fullname])
  end

  # If you have extra params to permit, append them to the sanitizer.
  def configure_account_update_params
    devise_parameter_sanitizer.permit(:account_update, keys: [
      :uid, :provider, :name, :fullname,
      :website, :introduction, :phone, :sex,
    ])
  end

  # ユーザー登録後、ユーザー個別ページへ 継承元をオーバーライド
  def after_sign_up_path_for(resource)
    user_path(resource)
  end

  # ユーザー編集後、ユーザー個別ページへ 継承元をオーバーライド
  def after_update_path_for(resource)
    user_path(resource)
  end

  # ユーザー削除後、ホームページへ 継承元をオーバーライド
  def after_sign_out_path_for(resource)
    home_path
  end
  # The path used after sign up for inactive accounts.
  # def after_inactive_sign_up_path_for(resource)
  #   super(resource)
  # end
  # #
  # private
  #
  #   def user_params
  #     params.require(:user).permit(:agreement, :avatar)
  #   end
end
