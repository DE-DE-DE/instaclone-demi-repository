class ApplicationController < ActionController::Base
  include ApplicationHelper
  include MethodsForControllers
  # https://qiita.com/shun-kusano/items/1a65040aae3e0db72294
  before_action :authenticate_user! # protect_from_forgery with: :exceptionより後におく。
  before_action :configure_permitted_parameters, if: :devise_controller?

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:account_update, keys: [:avatar, :avatar_cache])
  end
end
