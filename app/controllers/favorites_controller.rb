class FavoritesController < ApplicationController
  after_action :invoke_html_or_javascript

  def create
    @post = Post.find_by(id: params[:post_id])
    @favorite = current_user.favorites.build(post_id: params[:post_id])
    if @favorite.save
      # 通知
      @post.create_notification_like(current_user)
    else
    end
  end

  def destroy
    @favorite = current_user.favorites.find_by(id: params[:id])
    @post = @favorite.post
    @favorite.destroy
  end

  private

  def favorite_params
    params.require(:favorite).permit(:post_id)
  end
end
