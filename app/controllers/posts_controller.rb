class PostsController < ApplicationController
  after_action :invoke_html_or_javascript, only: [:show]

  def index
    # 全ての投稿写真の中から、フォローしているユーザーの投稿写真を取り出す
    @latest_posts = Post.latest(current_user.following)
    # ページネーション
    # 通常pageメソッドはActive Recordのオブジェクトにしか適用されないので次の書き方はエラー
    # @latest_posts = Post.latest(current_user.following).paginate(page: params[:page], per_page: 9)
    @latest_posts = Kaminari.paginate_array(@latest_posts).page(params[:page]).per(9)
  end

  def show
    @post = Post.find(params[:id])
    @comments = @post.comments
  end

  def new
    @post = Post.new
  end

  def create
    @post = current_user.posts.build(post_params)

    # @post.image.attach(params[:post][:image])
    if @post.save
      flash[:success] = "投稿しました"
      # リダイレクト先で自動でモーダル表示できるように、クエリパラメータに投稿写真のidを含める、
      redirect_to "/users/#{current_user.id}/?post_with_quick_modal=#{@post.id}"
    else
      flash[:danger] = "投稿できませんでした"
      render new_post_path
    end
  end

  def edit
    @post = Post.find(params[:id])
  end

  def update
    @post = current_user.posts.find_by(id: params[:id])
    if @post
      @post.attributes = post_params # 特定のカラムのみ代入し、DBには保存しない
      # @post.image.attach(params[:post][:image])
      if @post.save
        flash[:success] = "編集しました"
        # リダイレクト先で自動でモーダル表示できるように、クエリパラメータに投稿写真のidを含める、
        redirect_to "/users/#{current_user.id}/?post_with_quick_modal=#{@post.id}"
      else
        flash[:danger] = "編集できませんでした"
        render "edit"
      end
    else
      flash[:danger] = "他のユーザーの投稿は編集できません"
      redirect_to "/users/#{current_user.id}"
    end
  end

  def destroy
    @post = current_user.posts.find_by(id: params[:id])
    if @post
      @post.destroy
      flash[:success] = "投稿を削除しました"
    else
      flash[:danger] = "他のユーザーの投稿は削除できません"
    end
    redirect_to "/users/#{current_user.id}"
  end

  def search
    @posts = Post.search(params[:keyword]).page(params[:page]).per(9)
    @keyword = params[:keyword]
  end

  private

  def post_params
    params.require(:post).permit(:title, :description, :image, :image_cache)
  end
end
