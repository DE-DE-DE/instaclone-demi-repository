class CommentsController < ApplicationController
  after_action :invoke_html_or_javascript, only: [:new, :create, :edit, :update]

  def new
    @post = Post.find(params[:postid])
    @comment = current_user.comments.build
  end

  def create
    @comment = current_user.comments.build(comment_params)
    @comments = current_user.comments
    if @comment.save
      @state = "OK"
      @comment.post.create_notification_comment(current_user)
    else
      @state = "NG"
    end
  end

  def edit
    @post = Post.find(params[:postid])
    # @commentを受け取ってedit.js.erbを呼び出す
    @comment = current_user.comments.find_by(id: params[:id])
  end

  def update
    @comment = current_user.comments.find_by(id: params[:id])
    if @comment.update(comment_params)
      @state = "OK"
    else
      @state = "NG"
    end
  end

  def destroy
    @comment = current_user.comments.find_by(id: params[:id])
    @comments = current_user.comments
    @post = Post.find_by(id: params[:postid])
    if @comment.destroy
      @state = "OK"
    else
      @state = "NG"
    end
  end

  private

  def comment_params
    params.require(:comment).permit(:content, :post_id)
  end
end
