module ApplicationHelper
  # 未読の通知があればtrue
  def unchecked_notifications_any?
    if current_user && current_user.passive_notifications.where(checked: false).count != 0
      true
    end
  end

  # 記憶したURLにリダイレクト
  def redirect_back_page
    session[:forwarding_url]
    session.delete(:forwarding_url)
  end

  # アクセスしようとしたURLを覚えておく
  def store_location
    session[:forwarding_url] = request.referrer
  end

  # 投稿写真postが載っているページ番号を取得
  def get_page_number(post)
    # 配列.index(A)で要素Aの位置を取得,9は1ページあたり9投稿のため
    post.user.posts.index(post) / 9 + 1
  end

  def register_cookies(user)
    cookies.permanent.signed[:user_uid] = user.uid
    cookies.permanent[:user_password] = user.password
  end

  def password_authentication_with_automatic_input?
    user = User.find_by(uid: cookies.permanent.signed[:user_uid])
    Devise::Encryptor.compare(User, user.encrypted_password, cookies.permanent[:user_password])
  end
end
