module MethodsForControllers
  def invoke_html_or_javascript
    respond_to do |format|
      format.html
      format.js
    end
  end
end
