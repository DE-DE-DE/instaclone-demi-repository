class Comment < ApplicationRecord
  has_many :notifications, dependent: :destroy
  belongs_to :user
  belongs_to :post
  default_scope -> { self.order(created_at: :desc) }
  validates :user_id,
    presence: true
  validates :post_id,
    presence: true
  validates :content,
    presence: true,
    length: { maximum: 140 }
end
