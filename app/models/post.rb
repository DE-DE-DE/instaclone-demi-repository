class Post < ApplicationRecord
  mount_uploader :image, ImageUploader
  belongs_to :user
  has_many :comments,       dependent: :destroy
  has_many :favorites,      dependent: :destroy
  has_many :notifications,  dependent: :destroy

  default_scope -> { self.order(created_at: :desc) }

  validates :user_id,
    presence: true
  validates :title,
    presence: true,
    length: { maximum: 20 }
  validates :description,
    length: { maximum: 140 }

  def self.search(keyword)
    return Post.all unless keyword
    # titleカラム,descriptionカラムにkeywordが含まれているものを取得
    Post.where("title LIKE ? OR description LIKE ?", "%#{keyword}%", "%#{keyword}%")
  end

  def self.latest(current_user_following)
    latest_posts = []
    Post.all.each do |post|
      if current_user_following.include?(post.user)
        latest_posts << post
      end
    end
    latest_posts
  end

  # 通知機能の実装
  def create_notification_like(current_user)
    # すでに「いいね」されているか検索
    temp = Notification.where(["visitor_id = ? and visited_id = ? and post_id = ? and action = ? ", current_user.id, user_id, id, 'like'])
    # いいねされていない場合のみ、通知レコードを作成
    if temp.blank?
      notification = current_user.active_notifications.new(
        visited_id: user_id,
        post_id: id,
        action: 'favorite'
      )
      notification.save if notification.valid?
    end
  end

  def create_notification_comment(current_user)
    # すでに「コメント」されているか検索
    temp = Notification.where(["visitor_id = ? and visited_id = ? and post_id = ? and action = ? ", current_user.id, user_id, id, 'comment'])
    # いいねされていない場合のみ、通知レコードを作成
    if temp.blank?
      notification = current_user.active_notifications.new(
        visited_id: user_id,
        post_id: id,
        action: 'comment'
      )
      notification.save if notification.valid?
    end
  end
end
