class User < ApplicationRecord
  mount_uploader :avatar, AvatarUploader
  has_many :posts, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :favorites, dependent: :destroy

  has_many :active_relationships,    class_name: "Relationship",
                                    foreign_key: "follower_id",
                                      dependent: :destroy
  has_many :passive_relationships,   class_name: "Relationship",
                                    foreign_key: "followed_id",
                                      dependent: :destroy
  has_many :following,                  through: :active_relationships,
                                         source: :followed
  has_many :followers,                  through: :passive_relationships,
                                         source: :follower
  has_many :active_notifications,    class_name: 'Notification',
                                    foreign_key: 'visitor_id',
                                      dependent: :destroy
  has_many :passive_notifications,   class_name: 'Notification',
                                    foreign_key: 'visited_id',
                                      dependent: :destroy
  attr_accessor :agreement

  validates :name,
    presence: true,
    length: { maximum: 20 }

  validates :fullname,
    length: { maximum: 20 }

  VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i.freeze
  validates :email,
    presence: true,
    length: { maximum: 255 },
    format: { with: VALID_EMAIL_REGEX },
    uniqueness: true

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable,
         :omniauthable

  validates :password,
    presence: true,
    allow_nil: true

  # ユーザーをフォローする
  def follow(other_user)
    following << other_user
  end

  # ユーザーをフォロー解除する
  def unfollow(other_user)
    active_relationships.find_by(followed_id: other_user.id).destroy
  end

  # 現在のユーザーがフォローしてたらtrueを返す
  def following?(other_user)
    following.include?(other_user)
  end

  def adding?(post)
    favorites.map(&:post).include?(post)
  end

  # 通知機能の実装
  def create_notification_follow(current_user)
    temp = Notification.where(["visitor_id = ? and visited_id = ? and action = ? ", current_user.id, id, 'follow'])
    if temp.blank?
      notification = current_user.active_notifications.new(
        visited_id: id,
        action: 'follow'
      )
      notification.save if notification.valid?
    end
  end

  def self.find_for_oauth(auth)
    user = User.where(uid: auth.uid, provider: auth.provider).first
    unless user
      user = User.create(
        uid: auth.uid,
        provider: auth.provider,
        name: auth.info.name,
        email: auth.info.email,
        password: Devise.friendly_token[0, 20]
      )
    end
    user
  end
end
