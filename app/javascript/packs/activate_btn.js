// 利用規約にチェックを入れたら登録ボタンを押せるようにする
// jQueryの導入方法参考 Rails Tutorial 8.2.3
// 下記コードの実装参考 https://agohack.com/checked-button-disabled/

$(function(){
    // 初期状態のボタンは無効
    $("#btn1").prop("disabled", true);
    // チェックボックスの状態が変わったら（クリックされたら）
    $("input[type='checkbox']").on('change', function () {
        // チェックされているチェックボックスの数
        if ($(".chk:checked").length === 1) {
            // ボタン有効
            $("#btn1").prop("disabled", false);
        } else {
            // ボタン無効
            $("#btn1").prop("disabled", true);
        }
    });
});

