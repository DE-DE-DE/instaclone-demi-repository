//遷移前の通知ページでどの投稿を選択したか
const postSelectedOnANotificationsPage = document.getElementById('post_selected_on_a_notifications_page')

//そのidを取得 innerTextには@post_with_quick_modal.idを入れてある
postId = postSelectedOnANotificationsPage.innerText

//imgタグをクリック imgタグにはposted-content__imageクラスを付与してある
document.getElementById(`post-${postId}`).getElementsByClassName('posted-content__image')[0].click();