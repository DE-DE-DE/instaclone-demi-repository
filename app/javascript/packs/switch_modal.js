var photoModal = document.getElementsByClassName("-hidden")[0];
const userSection = document.getElementsByClassName("user-section")[0];
// const userSection = document.getElementsByClassName("container")[0];
const div = document.createElement("div");

//投稿画像を全て取得
const postedContentImages = document.getElementsByClassName('posted-content__image')

//クリックした投稿画像のみを抽出 クリックイベント発生でmodal関数を呼び出す
for( var $i = 0; $i < postedContentImages.length; $i++ ) {
    postedContentImages[$i].addEventListener('click', modal)
}

function modal(){
    // -hiddenクラスを取り除く モーダルが表示される
    photoModal.classList.remove("-hidden");
    // -hiddenクラスがない場合=モーダルが表示されている場合のみ処理
    if(!photoModal.classList.contains("-hidden")){
        // モーダルの下層にレイヤーを1枚挿入
        userSection.appendChild(div).classList.add("insertedlayer");
        // レイヤーのクリックイベントをキャッチ closeModal関数を呼び出す
        const insert = document.getElementsByClassName("insertedlayer")[0];
        insert.addEventListener('click', closeModal)
        //モーダルに-hiddenクラスを付与 モーダルを非表示 挿入していたレイヤーもなくす
        function closeModal(){
            var photoModal = document.getElementsByClassName("photomodal")[0];
            userSection.removeChild(div);
            photoModal.classList.add("-hidden");
        }
    }
}

