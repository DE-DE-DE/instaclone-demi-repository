

//利用規約ボタンを取得
const termsOpen = document.getElementById('terms-open')
//利用規約ボタンクリックでモーダルを開く
termsOpen.addEventListener('click', modal)


function modal(){
    // モーダルを取得 モーダルには-hiddenクラスを付けてある
    var termsModal = document.getElementsByClassName("-hidden")[0];
    // -hiddenクラスを取り払う モーダルが表示される
    termsModal.classList.remove("-hidden");


    // モーダルが表示されている場合のみif内に入る -hiddenクラスがないことが条件
    if(!termsModal.classList.contains("-hidden")){

        // モーダルの下層にレイヤーを1枚挿入
        const div = document.createElement("div");
        const signUpSection = document.getElementsByClassName("sign-up-section")[0];
        signUpSection.appendChild(div).classList.add("insertedlayer");

        // レイヤークリックでcloseModal関数を呼び出す
        const insert = document.getElementsByClassName("insertedlayer")[0];
        insert.addEventListener('click', closeModal)

        // 閉じるクリックでcloseModal関数を呼び出す
        const closebtn = document.getElementById("terms-close")
        closebtn.addEventListener('click', closeModal)

        //モーダルに-hiddenクラスを付与 モーダルを非表示 挿入していたレイヤーもなくす
        function closeModal(){
            var termsModal = document.getElementsByClassName("agreement-section")[0];
            signUpSection.removeChild(div);
            termsModal.classList.add("-hidden");
        }
    }
}

