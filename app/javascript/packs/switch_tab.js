
//---------------------------------------------------------------------------//
// 問題
// お気に入りタブを開いていても、ページネーションで次のページを表示した場合に、
// デフォルトのタブが選択されてしまい、お気に入りタブを選択しなおさないといけない。
// 対策
// セッションにそれまで開いていたタブの情報を貼り付けて、
// 次のページを表示したときに同じタブを保持する。

// セッションの値を取得
var activeTab = window.sessionStorage.getItem(['active_tab']);
// タブを取得
var leftTab = document.getElementById('my-posts-input');
var rightTab = document.getElementById('favorite-posts-input');
// セッションの値がMyタブを示していたとき
if(activeTab === 'my-posts') {
    // Myタブを自動選択(実体はcheckboxなのでcheckedで選択できる)
    leftTab.checked = true;
    // Myタブにactiveクラスを付与、お気に入りタブからactiveクラスを取り除く
    // このactiveクラスがついていれば写真一覧を表示できる(displayMyPostsTriggeredByActiveClassで実行する)
    leftTab.classList.add('active');
    rightTab.classList.remove('active');

}
// セッションの値がお気に入りタブを示していたとき
else if(activeTab === 'favorite-posts') {
    // お気に入りタブを自動選択(実体はcheckboxなのでcheckedで選択できる)
    rightTab.checked = true;
    // お気に入りタブにactiveクラスを付与、Myタブからactiveクラスを取り除く
    // このactiveクラスがついていれば写真一覧を表示できる(displayFavoritePostsTriggeredByActiveClassで実行する)
    rightTab.classList.add('active');
    leftTab.classList.remove('active');
}




//---------------------------------------------------------------------------//
const myPostsContent = document.getElementById("my-posts-content");
const favoritePostsContent = document.getElementById("favorite-posts-content");



//---------------------------------------------------------------------------//
// 実行条件：Myタブにactiveクラスが付いているとき
function displayMyPostsTriggeredByActiveClass(){
    // My写真一覧に切り替え
    myPostsContent.style.display ="block";
    favoritePostsContent.style.display ="none";
}
// 実行条件：お気に入りタブにactiveクラスが付いているとき
function displayFavoritePostsTriggeredByActiveClass(){
    // お気に入り写真一覧に切り替え
    myPostsContent.style.display ="none";
    favoritePostsContent.style.display ="block";
}

// activeクラスが付いている方の写真一覧を表示する
// My写真一覧
if(document.getElementById("my-posts-input").classList.contains("active")){
    displayMyPostsTriggeredByActiveClass();
}
//お気に入りの写真一覧
else if(document.getElementById("favorite-posts-input").classList.contains("active")) {
    displayFavoritePostsTriggeredByActiveClass();
}




//-------------------------------------クリック--------------------------------------//
// 実行条件：Myタブをクリック
function displayMyPostsTriggeredByClick(){
    // Myタブを選択中であることをセッションに知らせる
    window.sessionStorage.setItem(['active_tab'],['my-posts']);
    // My写真一覧に切り替え
    myPostsContent.style.display ="block";
    favoritePostsContent.style.display ="none";
}
// 実行条件：お気に入りタブをクリック
function displayFavoritePostsTriggeredByClick(){
    // お気に入りタブを選択中であることをセッションに知らせる
    window.sessionStorage.setItem(['active_tab'],['favorite-posts']);
    // お気に入り写真一覧に切り替え
    myPostsContent.style.display ="none";
    favoritePostsContent.style.display ="block";
}

// My投稿タブがクリックされたら
var myPosts = document.getElementById('my-posts-input');
myPosts.addEventListener('change', displayMyPostsTriggeredByClick);
// お気に入りの投稿タブがクリックされたら
var favoritePosts = document.getElementById('favorite-posts-input');
favoritePosts.addEventListener('change', displayFavoritePostsTriggeredByClick);