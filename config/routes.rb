Rails.application.routes.draw do
  devise_for :users, :controllers => {
      :registrations  => 'users/registrations',
      :sessions       => 'users/sessions',
      omniauth_callbacks: 'users/omniauth_callbacks'
  }

  devise_scope :user do
    get "sign_in",    to: "users/sessions#new"
    get "sign_out",   to: "users/sessions#destroy"
    get "users/:id",  to: "users/registrations#show"
    get "users",      to: "users/registrations#index"
  end

  get 'posts/search', to: 'posts#search'
  get '/home',        to: 'static_pages#home'
  get '/agreement',   to: 'static_pages#agreement'
  root "posts#index"

  resources :favorites,     only: [:create, :destroy]
  resources :posts
  resources :comments,      only: [:new, :create, :edit, :update, :destroy]
  resources :relationships, only: [:create, :destroy]
  resources :notifications, only: [:index]
  resources :users do
    member do
      get :following, :followers
      # GET /users/1/following
      # GET /users/1/followers
    end
  end
end
