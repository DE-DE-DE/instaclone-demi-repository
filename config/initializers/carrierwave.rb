require 'carrierwave/storage/abstract'
require 'carrierwave/storage/file'
require 'carrierwave/storage/fog'

CarrierWave.configure do |config|
  config.storage :fog
  config.fog_provider = 'fog/aws'
  config.fog_directory  = 'instaclone-group' # 作成したバケット名を記述
  config.fog_credentials = {
      provider: 'AWS',
      aws_access_key_id: Rails.application.credentials.aws[:access_key_id],
      aws_secret_access_key: Rails.application.credentials.aws[:secret_access_key],
      # aws_access_key_id: ENV["AWS_ACCESS_KEY_ID"], # 環境変数
      # aws_secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"], # 環境変数
      region: "us-east-2",   # オハイオを選択した場合
      path_style: true
  }
end

# CarrierWave.configure do |config|
#   if !Rails.env.test?
#     config.fog_credentials = {
#         provider: "AWS",
#         aws_access_key_id: ENV["AWS_ACCESS_KEY_ID"],
#         aws_secret_access_key: ENV["AWS_SECRET_ACCESS_KEY"]
#     }
#     if Rails.env.production?
#       config.fog_directory = ENV["PRODUCTION_S3_BUCKET"]
#     else
#       config.fog_directory = ENV["DEVELOPMENT_S3_BUCKET"]
#     end
#   end
# end